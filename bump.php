<?php
require_once('db.php');

$srange = 20000;
$trange = 1500;

$json = file_get_contents('php://input');
$jsonObj = json_decode($json, true);


$lat = $jsonObj['lat'];
$lon = $jsonObj['lon'];
//$time = $jsonObj['time'];
$time = round(microtime(true) * 1000);
// get user id based on fbID
$idresult = mysqli_query($con, "SELECT id_users FROM users WHERE fbID_users = \"" . $jsonObj['fbID'] . "\"");
$idfetch = mysqli_fetch_array($idresult);
$id = $idfetch['id_users'];

$query = 
	"SELECT id_users FROM bumps
	WHERE time_bumps > " . ($time - $trange) .
	" AND lat_bumps BETWEEN " . ($lat-$srange) . " AND " . ($lat+$srange) .
	" AND long_bumps BETWEEN " . ($lon-$srange) . " AND " . ($lon+$srange) . 
	" AND id_users != " . $id;
	
// query nearby bumps
$result = mysqli_query($con, $query);

if(mysqli_num_rows($result) == 0) { // no bumps found
	$response["successquery"] = false;
} else { // bumps found
	$response["successquery"] = true;
	$idfriendfetch = mysqli_fetch_array($result);
	$idfriend = $idfriendfetch['id_users'];
	
	$idresultfriend = mysqli_query($con, "SELECT fbID_users FROM users WHERE id_users = \"" . $idfriend . "\"");
	$idfetchfri = mysqli_fetch_array($idresultfriend);
	$idfri = $idfetchfri['fbID_users'];
	 
	$inscon = mysqli_query($con, // insert connection between users
		"INSERT INTO connections (id_users, id_users_friend)
		VALUES ('" . $id . "', '" . $idfriend . "')"
	);
	$inscon2 = mysqli_query($con, // insert connection between users
		"INSERT INTO connections (id_users, id_users_friend)
		VALUES ('" . $idfriend . "', '" . $id . "')"
	);
		
	if($inscon && $inscon2) { // sucess if connection inserted
		$response["fbID"] = $idfri;
	} 
	
}

// insert bump into database
$insbum = mysqli_query($con,
	"INSERT INTO bumps (lat_bumps, long_bumps, time_bumps, id_users)
	VALUES ('" . $lat . "', '" . $lon . "', '" . $time . "', '" . $id . "')"
);
	
if($insbum) {
    $response["success"] = true;
} else
    $response["success"] = false;

echo json_encode($response);

mysqli_free_result($result);
 
?>