<?php
require_once('db.php');

$json = file_get_contents('php://input');
$jsonObj = json_decode($json, true);
 
// get user id based on fbID
$idresult = mysqli_query($con, "SELECT id_users FROM users WHERE fbID_users = \"" . $jsonObj['fbID'] . "\"");
$idfetch = mysqli_fetch_array($idresult);
$id = $idfetch['id_users']; 

$result = mysqli_query($con,
	"SELECT name_users, fbID_users FROM connections
	JOIN users ON connections.id_users_friend=users.id_users
	WHERE connections.id_users = " . $id);
	
    if(mysqli_num_rows($result) == 0) { // no record found
		$response["success"] = false;
	} else { // record found
	    $response["success"] = true;
		$connections = array();
		
		//iterate thought result
		while($row = mysqli_fetch_array($result)) {
			$connections[] = array(
				'name' => $row['name_users'], // add fbIDs of connections
				'fbID' => $row['fbID_users'] // add fbIDs of connections
			);
		}
	    $response["connections"] = $connections;
	}
	
	echo json_encode($response);
	
    mysqli_free_result($result);
 
?>